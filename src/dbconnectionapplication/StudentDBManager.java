/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbconnectionapplication;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Axelrd2
 */
public class StudentDBManager {
    private Connection dbconnection = null;

    public ArrayList<Student> insertStudent() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public static class Student{
        public String name;
        public String city;
        public int age;
        public int id;
    }
    
    public StudentDBManager(Connection conn){
        dbconnection = conn;
    }
    
    public Student getStudentByID(int sID){
        Student student = null;
        try {
            Statement query = dbconnection.createStatement();
            String sqlquery = "SELECT * FROM alumne WHERE idalumne = '"+sID+"';";
            ResultSet result = query.executeQuery(sqlquery);
            if (result.next()){
                student = new Student();
                student.id = sID;
                student.name = result.getString("nom");
                student.city = result.getString("ciutat");
                student.age = result.getInt("edat");
            
            }
            result.close();
            query.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return student;
    }
    
    public ArrayList<Student> getStudentsByName(String sNom) {
        Student student = null;
        ArrayList<Student> nom = new ArrayList();
        
        try {
            Statement query = dbconnection.createStatement();
            String sqlquery = "SELECT * FROM alumne WHERE nom = '"+sNom+"';";
            ResultSet result = query.executeQuery(sqlquery);
            if (result.next()){
                student = new Student();
                student.id = result.getInt("idalumne");
                student.name = sNom;
                student.city = result.getString("ciutat");
                student.age = result.getInt("edat");
                nom.add(student);
            }
            result.close();
            query.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nom;
    }
    
    public ArrayList<Student> getStudentsByCity(String sCity) {
        Student student = null;
        ArrayList<Student> ciutat = new ArrayList();
        
        try {
            Statement query = dbconnection.createStatement();
            String sqlquery = "SELECT * FROM alumne WHERE ciutat = '"+sCity+"';";
            ResultSet result = query.executeQuery(sqlquery);
            while (result.next()){
                student = new Student();
                student.id = result.getInt("idalumne");
                student.name = result.getString("nom");
                student.city = sCity;
                student.age = result.getInt("edat");
                ciutat.add(student);
            }
            result.close();
            query.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ciutat;
    }
    
    public ArrayList<Student> getStudentsByAge(int sAge) {
        Student student = null;
        ArrayList<Student> edat = new ArrayList();
        
        try {
            Statement query = dbconnection.createStatement();
            String sqlquery = "SELECT * FROM alumne WHERE edat = "+sAge+";";
            ResultSet result = query.executeQuery(sqlquery);
            if (result.next()){
                student = new Student();
                student.id = result.getInt("idalumne");
                student.name = result.getString("nom");
                student.city = result.getString("ciutat");
                student.age = sAge;
                edat.add(student);
            }
            result.close();
            query.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return edat;
    }
    
    public ArrayList<Student> getAllStudents() {
        Student student = null;
        ArrayList<Student> tots = new ArrayList();
        try {
            Statement query = dbconnection.createStatement();
            String sqlquery = "SELECT * FROM alumne;";
            ResultSet result = query.executeQuery(sqlquery);
            while (result.next()){
                student = new Student();
                student.id = result.getInt("idalumne");
                student.name = result.getString("nom");
                student.city = result.getString("ciutat");
                student.age = result.getInt("edat");
                tots.add(student);
            }
            result.close();
            query.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tots;
    }
    
    public boolean insertStudent(Student student){
        int correct = 0;
        
        try {
            Statement query = dbconnection.createStatement();
            String sqlquery = "INSERT INTO alumne (nom,ciutat,edat) VALUES('"+student.name+"', '"+student.city+"', "+student.age+");";
            
            correct = query.executeUpdate(sqlquery);
            query.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (correct == 1);
    }
    
    public boolean updateStudent(Student student){
        int correct = 0;
        try {
            Statement query = dbconnection.createStatement();
            String sqlquery = "UPDATE alumne SET nom = '"+student.name+"', ciutat = '"+student.city+"', edat = '"+student.age+"' WHERE idalumne = "+student.id+";";
            
            correct = query.executeUpdate(sqlquery);
            query.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (correct == 1);
    }
    
    public boolean deleteStudent(Student student){
        int correct = 0;
        
        try {
            Statement query = dbconnection.createStatement();
            String sqlquery = "DELETE FROM alumne WHERE idalumne = "+student.id+";";
            
            correct = query.executeUpdate(sqlquery);
            query.close();
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (correct == 1);
    }
    
}
