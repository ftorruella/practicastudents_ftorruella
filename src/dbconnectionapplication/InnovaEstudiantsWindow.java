package dbconnectionapplication;

import dbconnectioncontroller.DBConnectionController;
import java.util.ArrayList;

public class InnovaEstudiantsWindow extends javax.swing.JFrame {
    private DBConnectionController controller;

    public InnovaEstudiantsWindow(DBConnectionController ctrl) {
        initComponents();
        controller = ctrl;
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        jButtonBuscar = new javax.swing.JButton();
        jTextFieldID = new javax.swing.JTextField();
        jLabelEstudiante = new javax.swing.JLabel();
        jLabelNombre = new javax.swing.JLabel();
        jLabelCiudad = new javax.swing.JLabel();
        jLabelEdad = new javax.swing.JLabel();
        jTextFieldEstudiante = new javax.swing.JTextField();
        jTextFieldNombre = new javax.swing.JTextField();
        jTextFieldCiudad = new javax.swing.JTextField();
        jTextFieldEdad = new javax.swing.JTextField();
        jLabelIdentificador = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        Buscar = new javax.swing.JButton();
        jTextFieldCBox = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButtonSortir = new javax.swing.JButton();
        jButtonInsertar = new javax.swing.JButton();
        jButtonEditar = new javax.swing.JButton();
        jButtonEliminar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Estudiante", "Nombre", "Ciudad", "Edad", "Todos" }));

        jButtonBuscar.setText("Buscar");
        jButtonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBuscarActionPerformed(evt);
            }
        });

        jLabelEstudiante.setText("Estudiante");

        jLabelNombre.setText("Nombre");

        jLabelCiudad.setText("Ciudad");

        jLabelEdad.setText("Edad");

        jTextFieldEstudiante.setEditable(false);

        jLabelIdentificador.setText("Identificador");

        Buscar.setText("Buscar");
        Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BuscarActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jButtonSortir.setText("Salir");
        jButtonSortir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSortirActionPerformed(evt);
            }
        });

        jButtonInsertar.setText("Insertar");
        jButtonInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonInsertarActionPerformed(evt);
            }
        });

        jButtonEditar.setText("Editar");
        jButtonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditarActionPerformed(evt);
            }
        });

        jButtonEliminar.setText("Eliminar");
        jButtonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 392, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jTextFieldCBox, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(63, 63, 63)
                                        .addComponent(Buscar)))
                                .addGap(18, 18, 18))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabelEstudiante, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabelNombre)
                                    .addComponent(jLabelCiudad)
                                    .addComponent(jLabelEdad))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jTextFieldCiudad, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 186, Short.MAX_VALUE)
                                    .addComponent(jTextFieldNombre, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextFieldEstudiante)
                                    .addComponent(jTextFieldEdad))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jButtonInsertar)
                                    .addComponent(jButtonEditar)
                                    .addComponent(jButtonEliminar)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabelIdentificador, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jTextFieldID, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonBuscar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 74, Short.MAX_VALUE)
                                .addComponent(jButtonSortir)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSeparator2)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonBuscar)
                    .addComponent(jLabelIdentificador, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSortir))
                .addGap(18, 18, 18)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelEstudiante)
                    .addComponent(jTextFieldEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonInsertar))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNombre)
                    .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonEditar))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelCiudad)
                    .addComponent(jTextFieldCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonEliminar))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelEdad)
                    .addComponent(jTextFieldEdad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldCBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Buscar))
                .addGap(28, 28, 28)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(48, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBuscarActionPerformed
        int id = Integer.parseInt(jTextFieldID.getText());
        
        StudentDBManager.Student student = controller.getStudentByID(id);
            
        jTextFieldEstudiante.setText("");
        jTextFieldNombre.setText("");
        jTextFieldCiudad.setText("");
        jTextFieldEdad.setText("");
        
        jTextFieldEstudiante.setText(String.valueOf(student.id));
        jTextFieldNombre.setText(student.name);
        jTextFieldCiudad.setText(student.city);
        jTextFieldEdad.setText(String.valueOf(student.age));
        
    }//GEN-LAST:event_jButtonBuscarActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        boolean disconnected = DBConnection.disconnect();
       
        if(disconnected) {
            System.out.println("Tot correcte!");
        }
    }//GEN-LAST:event_formWindowClosing

    private void BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BuscarActionPerformed
        String sel = (String)jComboBox1.getSelectedItem();
        //String value = jComboBox1.getSelectedItem().toString();

        if (sel.equals("Estudiante")) {
             jTextArea1.setText("");
            String estud = jTextFieldCBox.getText();
            int id = Integer.parseInt(estud);
           
            
            StudentDBManager.Student students = controller.getStudentByID(id);
                
            jTextArea1.setText("Estudiante: "+students.id+"   Nombre: "+students.name+"   Ciudad: "+students.city+"   Edad: "+students.age+"\n");
            
        } else if (sel.equals("Nombre")) {
            jTextArea1.setText("");
            String nombre = jTextFieldCBox.getText();
            ArrayList<StudentDBManager.Student> students = controller.getStudentsByName(nombre);
                
            for (int i=0; i<students.size() ;i++ ){
                jTextArea1.append("Estudiante: "+students.get(i).id+"   Nombre: "+students.get(i).name+"   Ciudad: "+students.get(i).city+"   Edad: "+students.get(i).age+"\n");
                }
            
        } else if (sel.equals("Ciudad")) {
            jTextArea1.setText("");
            String ciudad = jTextFieldCBox.getText();
            ArrayList<StudentDBManager.Student> students = controller.getStudentsByCity(ciudad);
              
            System.out.println("Estudiants totals: "+students.size());
            
            for (int i=0; i<students.size() ;i++ ){
                jTextArea1.append("Estudiante: "+students.get(i).id+"   Nombre: "+students.get(i).name+"   Ciudad: "+students.get(i).city+"   Edad: "+students.get(i).age+"\n");
                }
            
        } else if (sel.equals("Edad")) {
            jTextArea1.setText("");
            String edad = jTextFieldCBox.getText();           
            int id = Integer.parseInt(edad);
            
            ArrayList<StudentDBManager.Student> students = controller.getStudentsByAge(id);
                
            for (int i=0; i<students.size() ;i++ ){
                jTextArea1.append("Estudiante: "+students.get(i).id+"   Nombre: "+students.get(i).name+"   Ciudad: "+students.get(i).city+"   Edad: "+students.get(i).age+"\n");
                }
            
        } else if (sel.equals("Todos")) {
            jTextArea1.setText("");
            
            ArrayList<StudentDBManager.Student> students = controller.getAllStudents();
            
            for (int i=0; i<students.size() ;i++ ){
                jTextArea1.append("Estudiante: "+students.get(i).id+"   Nombre: "+students.get(i).name+"   Ciudad: "+students.get(i).city+"   Edad: "+students.get(i).age+"\n");
                }
            
        }
        
        
    }//GEN-LAST:event_BuscarActionPerformed

    private void jButtonSortirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSortirActionPerformed
        setVisible(false);
        new InnovaMainWindow(controller).setVisible(true);
    }//GEN-LAST:event_jButtonSortirActionPerformed

    private void jButtonInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonInsertarActionPerformed
        // TODO add your handling code here:            
        StudentDBManager.Student student = new StudentDBManager.Student();
        
        student.id = Integer.parseInt(jTextFieldID.getText());
        student.name = jTextFieldNombre.getText();
        student.city = jTextFieldCiudad.getText();
        student.age = Integer.parseInt(jTextFieldEdad.getText());
                
        controller.insertStudent(student);
            
    }//GEN-LAST:event_jButtonInsertarActionPerformed

    private void jButtonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEliminarActionPerformed
        // TODO add your handling code here:
        StudentDBManager.Student student = new StudentDBManager.Student();
        
        student.id = Integer.parseInt(jTextFieldID.getText());
        student.name = jTextFieldNombre.getText();
        student.city = jTextFieldCiudad.getText();
        student.age = Integer.parseInt(jTextFieldEdad.getText());
                
        controller.deleteStudent(student);
    }//GEN-LAST:event_jButtonEliminarActionPerformed

    private void jButtonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditarActionPerformed
        // TODO add your handling code here:
        StudentDBManager.Student student = new StudentDBManager.Student();
        
        student.id = Integer.parseInt(jTextFieldID.getText());
        student.name = jTextFieldNombre.getText();
        student.city = jTextFieldCiudad.getText();
        student.age = Integer.parseInt(jTextFieldEdad.getText());
                
        controller.updateStudent(student);
    }//GEN-LAST:event_jButtonEditarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InnovaEstudiantsWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InnovaEstudiantsWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InnovaEstudiantsWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InnovaEstudiantsWindow.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new InnovaEstudiantsWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Buscar;
    private javax.swing.JButton jButtonBuscar;
    private javax.swing.JButton jButtonEditar;
    private javax.swing.JButton jButtonEliminar;
    private javax.swing.JButton jButtonInsertar;
    private javax.swing.JButton jButtonSortir;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabelCiudad;
    private javax.swing.JLabel jLabelEdad;
    private javax.swing.JLabel jLabelEstudiante;
    private javax.swing.JLabel jLabelIdentificador;
    private javax.swing.JLabel jLabelNombre;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextFieldCBox;
    private javax.swing.JTextField jTextFieldCiudad;
    private javax.swing.JTextField jTextFieldEdad;
    private javax.swing.JTextField jTextFieldEstudiante;
    private javax.swing.JTextField jTextFieldID;
    private javax.swing.JTextField jTextFieldNombre;
    // End of variables declaration//GEN-END:variables
}
