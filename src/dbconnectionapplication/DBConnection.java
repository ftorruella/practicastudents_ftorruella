/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbconnectionapplication;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author macervero
 */
public class DBConnection {
    private static Connection dbconnection = null;
    private static boolean connected = false;
    private static String dburl = "";
    private static String dbuser = "";
    private static String dbpasswd = "";
    private static String dbname = "";
    
    public static void setCredentials(String url, String user,
            String passwd, String name) {
        if(!connected) {
            dburl = url;
            dbuser = user;
            dbpasswd = passwd;
            dbname = name;
        }
    }
    
    public static Connection connect() {
        if(!connected) {
            try {
                String url = "jdbc:postgresql://" + dburl +
                        "/" + dbname;
                System.out.println(url);
                dbconnection = DriverManager.getConnection(url,
                        dbuser, dbpasswd);
                connected = true;
            } catch (SQLException e) {
                dbconnection = null;
                connected = false;
                
                e.printStackTrace();
            }
        }
        return dbconnection;
    }
    
    public static boolean disconnect() {
        if(connected) {
            try {
                dbconnection.close();
                connected = false;
                return true;
            } catch (SQLException e) {
                connected = true;
                
                e.printStackTrace();
            }
        }
        return false;
    }
    
}
