package dbconnectioncontroller;

import dbconnectionapplication.DBConnection;
import dbconnectionapplication.StudentDBManager;
import dbconnectionapplication.StudentDBManager.Student;
import java.sql.Connection;
import java.util.ArrayList;

public class DBConnectionController {
    
    private Connection dbconnection;
    private StudentDBManager dbstudent = null;
    
    public boolean login(String username, String passwd){
        DBConnection.setCredentials("192.168.239.133:5432",
                username, passwd, "institutdb");
        
        dbconnection = DBConnection.connect();
        if (dbconnection != null){
            dbstudent = new StudentDBManager(dbconnection);
        }
        
        return (dbconnection != null);
    }
    
    public Student getStudentByID(int sID){
        return dbstudent.getStudentByID(sID);
    }
    
    public ArrayList<Student> getStudentsByName(String sNom){
        return dbstudent.getStudentsByName(sNom);
    }
    
    public ArrayList<Student> getStudentsByCity(String sCiutat){
        return dbstudent.getStudentsByCity(sCiutat);
    }
    
    public ArrayList<Student> getStudentsByAge(int sAge){
        return dbstudent.getStudentsByAge(sAge);
    }
    
    public ArrayList<Student> getAllStudents(){
        return dbstudent.getAllStudents();
    }
    
    public boolean insertStudent(Student student){
        return dbstudent.insertStudent(student);
    }
    
    public boolean updateStudent(Student student){
        return dbstudent.updateStudent(student);
    }
    
    public boolean deleteStudent(Student student){
        return dbstudent.deleteStudent(student);
    }


}
